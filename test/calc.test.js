const expect = require('chai').expect;
const {add} = require('../src/calc');

describe("add", () => {
    it("1st add", () => {
        expect(add(1 , 5)).to.equal(6);
    });
    it("Should return the result of addition", () => {
        expect(add(0.1 , 0.7)).to.equal(0.8);
    });
    it("Should return the result of addition", () => {
        expect(add(0.000000000000000000000001 , 0.000000000000000000000007)).to.equal(0.000000000000000000000008);
    });
    it("Should return the result of addition", () => {
        expect(add(0.0000000000000007 , 0.1 )).to.equal(0.1000000000000007);
    });
    it("Empty Parameters", () => {
        expect(add()).to.equal("Error! Empty parameters");
    });
    it("Letters  in parameters", () => {
        expect(add("jadz" , "dzq")).to.equal("Error!");
    });
})
