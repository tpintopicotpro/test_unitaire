// const { exports } = require("mocha/lib/interfaces");

const fizzbuzz = (num) => {
    return (num % 5 == 0 && num % 7 == 0? "fizzbuzz": num == null? "Error!" : typeof(num) !== "number"? "Error!": num % 5 == 0 ? "buzz" : (num % 7 == 0?"fizz" : ""));
}

exports.fizzbuzz = fizzbuzz;